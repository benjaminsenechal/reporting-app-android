package synergiaclient.reportingapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;


public class FormActivity extends Activity implements View.OnClickListener {
    private static ServiceTest service;
    private static final int REQUEST_CAMERA = 1888;
    private static final int BUTTON_POSITIVE = -1;
    private static final int SELECT_FILE = 1887;
    private ArrayAdapter<String> reportTypeList;
    private ArrayList<ReportType> reportTypes;
    private ReportType currentReportType = null;
    private Map<String, Object> dictionary;
    private Button buttonType;
    private Button buttonPicture1;
    private Button buttonPicture2;
    private Button buttonPicture3;
    private Button buttonSend;
    private ProgressBar progressBarForm;
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private TextView textViewDescriptionApp;
    private TextView textViewDescriptionAlertApp;
    private CheckBox checkBoxPriority;
    private EditText editTextNumero;
    private EditText editTextAdress;
    private EditText editTextDescription;
    private int selectedImage;
    private float currentLatitudeUser;
    private float currentLongitudeUser;
    private File currentFile;
    private HashMap<String, File> images;
    private CheckBox beNotified;
    private EditText editTextEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        this.setTitle("Signaler");

        images = new HashMap<String, File>();


        reportTypeList = new ArrayAdapter<String>(
                FormActivity.this,
                android.R.layout.select_dialog_singlechoice);

        loadReportType();

        progressBarForm = (ProgressBar)findViewById(R.id.progressBarForm);

        textViewDescriptionApp = (TextView)findViewById(R.id.textViewDescriptionApp);
        textViewDescriptionApp.setText("Vous pouvez signaler les incidents rencontrés aussi bien sur le réseau public que sur votre propre installation. Les signalements privés ne seront pas visible par les autres utilisateurs.");

        textViewDescriptionAlertApp = (TextView)findViewById(R.id.textViewDescriptionAlertApp);
        textViewDescriptionAlertApp.setText("Attention, cela ne remplace pas un contact téléphonique. Le traitement ne se fait pas en temps réel.");

        editTextAdress = (EditText)findViewById(R.id.editTextAdress);

        checkBoxPriority = (CheckBox)findViewById(R.id.checkBoxPriority);
        checkBoxPriority.setOnClickListener(this);

        beNotified = (CheckBox)findViewById(R.id.beNotified);
        beNotified.setOnClickListener(this);

        editTextEmail = (EditText)findViewById(R.id.editTextEmail);
        editTextEmail.setVisibility(View.GONE);


        editTextNumero = (EditText)findViewById(R.id.editTextNumero);
        editTextNumero.setVisibility(View.GONE);

        editTextDescription = (EditText)findViewById(R.id.editTextDescription);

        buttonType = (Button)findViewById(R.id.buttonType);
        buttonType.setOnClickListener(this);

        buttonPicture1 = (Button)findViewById(R.id.buttonPicture1);
        buttonPicture1.setOnClickListener(this);

        buttonPicture2 = (Button)findViewById(R.id.buttonPicture2);
        buttonPicture2.setOnClickListener(this);

        buttonPicture3 = (Button)findViewById(R.id.buttonPicture3);
        buttonPicture3.setOnClickListener(this);

        buttonSend = (Button)findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(this);

        imageView1 = (ImageView)findViewById(R.id.imageView);
        imageView2 = (ImageView)findViewById(R.id.imageView2);
        imageView3 = (ImageView)findViewById(R.id.imageView3);

        SharedPreferences preferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        currentLatitudeUser = preferences.getFloat("currentLatitudeUser", 0.0f);
        currentLongitudeUser = preferences.getFloat("currentLongitudeUser", 0.0f);

        if (currentLatitudeUser == 0.0 && currentLongitudeUser == 0.0)
        {
            alertWithText("Votre position n'a pas pu être calculée");
        }
    }

    public void setUpCheckBox()
    {
        if (checkBoxPriority.isChecked()) {
            editTextNumero.setVisibility(View.VISIBLE);
        } else {
            editTextNumero.setVisibility(View.GONE);
        }

        if (beNotified.isChecked())
        {
            editTextEmail.setVisibility(View.VISIBLE);
        } else {
            editTextEmail.setVisibility(View.GONE);
        }
    }

    public void loadReportType() {
        service = RetrofitUtils.GetRestAdapter().create(ServiceTest.class);
        service.listReportTypes(new Callback<ArrayList<ReportType>>() {
            @Override
            public void success(ArrayList<ReportType> reportTypesReceived, Response response) {
                reportTypes = reportTypesReceived;
                for (ReportType reportType: reportTypesReceived)
                {
                    reportTypeList.add(reportType.getName());
                }
            }

        @Override
            public void failure(RetrofitError error) {
                String name = error.toString();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.buttonType : {
                showDialogReportType();
                break;
            }
            case R.id.buttonPicture1 : {
                selectedImage = 1;
                selectImage();
                break;
            }
            case R.id.buttonPicture2 : {
                selectedImage = 2;
                selectImage();
                break;
            }
            case R.id.buttonPicture3 : {
                selectedImage = 3;
                selectImage();
                break;
            }
            case R.id.buttonSend : {
                sendReport();
            }
            case R.id.checkBoxPriority : {
                setUpCheckBox();
                break;
            }

            case R.id.beNotified : {
                setUpCheckBox();
                break;
            }
            default: {
                break;
            }
        }
    }

    public void sendReport() {
        progressBarForm.setVisibility(ProgressBar.VISIBLE);
        buttonSend.setEnabled(false);
        service = RetrofitUtils.GetRestAdapter().create(ServiceTest.class);
/*
        TypedFile currentFileD = new TypedFile("image/png", currentFile);
        Map<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("name", editTextTitle.getText().toString());
        dictionary.put("description", editTextDescription.getText().toString());
        dictionary.put("latitude", currentLatitudeUser);
        dictionary.put("longitude", currentLongitudeUser);
        dictionary.put("report_type_id", currentReportType.getId());
        dictionary.put("priority", checkBoxPriority.isChecked());
        dictionary.put("reporter_numero", editTextNumero.getText().toString());
        dictionary.put("pictures_attributes[0][path]", currentFileD);
        dictionary.put("pictures_attributes[0][path]", currentFileD);
*/
        if (checkIfValid()) {
            service.sendReport(dictionary, new Callback<String>() {
                @Override
                public void success(String s, Response response) {
                    buttonSend.setEnabled(true);
                    progressBarForm.setVisibility(ProgressBar.GONE);
                    alertWithText("Signalement envoyé avec succès");

                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getResponse().getStatus() == 200)
                    {
                        buttonSend.setEnabled(true);
                        progressBarForm.setVisibility(ProgressBar.GONE);
                        alertWithText("Signalement envoyé avec succès");
                    } else {
                        buttonSend.setEnabled(true);
                        progressBarForm.setVisibility(ProgressBar.GONE);
                        alertWithText("Une erreur est survenue");
                    }
                }
            });
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Veuillez remplir tous les champs");
            builder1.setCancelable(true);
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    public void alertWithText(String text)
    {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(text);
        builder1.setCancelable(true);
        AlertDialog alert11 = builder1.create();
        alert11.setButton(BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert11.show();
    }

    Boolean checkIfValid()
    {
        Boolean bool;
        dictionary = new HashMap<String, Object>();

        if (beNotified.isChecked()){
            if (checkBoxPriority.isChecked())
            {
                if (!editTextEmail.getText().toString().matches("") && !editTextDescription.getText().toString().matches("") && !editTextNumero.getText().toString().matches("") && currentReportType != null) {
                    dictionary.put("description", editTextDescription.getText().toString());
                    dictionary.put("latitude", currentLatitudeUser);
                    dictionary.put("longitude", currentLongitudeUser);
                    dictionary.put("report_type_id", currentReportType.getId());
                    dictionary.put("priority", checkBoxPriority.isChecked());
                    dictionary.put("reporter_adress", editTextAdress.getText().toString());
                    dictionary.put("reporter_numero", editTextNumero.getText().toString());
                    dictionary.put("reporter_email", editTextEmail.getText().toString());

                    int i = 0;
                    for (File image : images.values() ) {
                        TypedFile currentFileD = new TypedFile("image/png", image);
                        String path = "pictures_attributes["+i+"][path]";
                        dictionary.put(path, currentFileD);
                        i++;
                    }

                    bool = true;
                } else {
                    bool = false;
                }
            } else {
                if (!editTextEmail.getText().toString().matches("") && !editTextDescription.getText().toString().matches("") && currentReportType != null) {
                    Log.i("currentReportType",currentReportType.toString());
                    dictionary.put("description", editTextDescription.getText().toString());
                    dictionary.put("latitude", currentLatitudeUser);
                    dictionary.put("longitude", currentLongitudeUser);
                    dictionary.put("report_type_id", currentReportType.getId());
                    dictionary.put("priority", checkBoxPriority.isChecked());
                    dictionary.put("reporter_email", editTextEmail.getText().toString());
                    dictionary.put("reporter_adress", editTextAdress.getText().toString());

                    int i = 0;
                    for (File image : images.values() ) {
                        TypedFile currentFileD = new TypedFile("image/png", image);
                        String path = "pictures_attributes["+i+"][path]";
                        dictionary.put(path, currentFileD);
                        i++;
                    }

                    bool = true;
                } else {
                    bool = false;
                }
            }
        } else {
            if (checkBoxPriority.isChecked())
            {
                if (!editTextDescription.getText().toString().matches("") && !editTextNumero.getText().toString().matches("") && currentReportType != null) {
                    dictionary.put("description", editTextDescription.getText().toString());
                    dictionary.put("latitude", currentLatitudeUser);
                    dictionary.put("longitude", currentLongitudeUser);
                    dictionary.put("report_type_id", currentReportType.getId());
                    dictionary.put("priority", checkBoxPriority.isChecked());
                    dictionary.put("reporter_numero", editTextNumero.getText().toString());
                    dictionary.put("reporter_adress", editTextAdress.getText().toString());

                    int i = 0;
                    for (File image : images.values() ) {
                        TypedFile currentFileD = new TypedFile("image/png", image);
                        String path = "pictures_attributes["+i+"][path]";
                        dictionary.put(path, currentFileD);
                        i++;
                    }

                    bool = true;
                } else {
                    bool = false;
                }
            } else {
                if (!editTextDescription.getText().toString().matches("") && currentReportType != null) {
                    Log.i("currentReportType",currentReportType.toString());
                    dictionary.put("description", editTextDescription.getText().toString());
                    dictionary.put("latitude", currentLatitudeUser);
                    dictionary.put("longitude", currentLongitudeUser);
                    dictionary.put("report_type_id", currentReportType.getId());
                    dictionary.put("priority", checkBoxPriority.isChecked());
                    dictionary.put("reporter_adress", editTextAdress.getText().toString());

                    int i = 0;
                    for (File image : images.values() ) {
                        TypedFile currentFileD = new TypedFile("image/png", image);
                        String path = "pictures_attributes["+i+"][path]";
                        dictionary.put(path, currentFileD);
                        i++;
                    }

                    bool = true;
                } else {
                    bool = false;
                }
            }
        }




        return bool;
    }

    public void showDialogReportType() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                FormActivity.this);
        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Selectionnez :");

        final ArrayAdapter<String> arrayAdapter = reportTypeList;

        builderSingle.setNegativeButton("Annuler",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonType.setText(arrayAdapter.getItem(which));
                        currentReportType = reportTypes.get(which);
                        dialog.dismiss();
                    }
                });
        builderSingle.show();
    }

    private void selectImage() {
        final CharSequence[] items = { "Prendre une photo", "Depuis ma galerie", "Annuler" };

        AlertDialog.Builder builder = new AlertDialog.Builder(FormActivity.this);
        builder.setTitle("Selectionnez :");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Prendre une photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment
                            .getExternalStorageDirectory(), "temp.png");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Depuis ma galerie")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Selectionnez un fichier"), SELECT_FILE);
                } else if (items[item].equals("Annuler")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bm = null;

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory().toString());

                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.png")) {
                        f = temp;
                        break;
                    }
                }
                BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                btmapOptions.inSampleSize = 4;
                bm = BitmapFactory.decodeFile(f.getAbsolutePath(),
                        btmapOptions);

                String path = android.os.Environment
                        .getExternalStorageDirectory()
                        + File.separator
                        + "Reports" + File.separator + "default";

                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), String.valueOf(System
                        .currentTimeMillis()) + ".png");
                currentFile = file;
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String tempPath = getPath(selectedImageUri, FormActivity.this);
                currentFile = new File(tempPath);
                BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                bm = BitmapFactory.decodeFile(tempPath, btmapOptions);
            }
        }

        convertFile(currentFile, bm);
        setUpImagePreview(bm, currentFile);
    }

    public void convertFile(File file, Bitmap bm) {
        try {
            OutputStream fOut = null;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 50, bos);
            byte[] bitmapdata = bos.toByteArray();
            fOut = new FileOutputStream(file);
            fOut.write(bitmapdata);
            fOut.flush();
            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getPath(Uri uri, Activity activity) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void setUpImagePreview(Bitmap m, File f){
        switch (selectedImage) {
            case 1: {
                images.put("1", f);
                imageView1.setImageBitmap(m);
                break;
            }
            case 2: {
                images.put("2", f);
                imageView2.setImageBitmap(m);
                break;
            }
            case 3: {
                images.put("3", f);
                imageView3.setImageBitmap(m);
                break;
            }
            default: {
                break;
            }
        }
    }
}