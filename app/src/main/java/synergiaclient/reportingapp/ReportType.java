package synergiaclient.reportingapp;

/**
 * Created by benjaminsenechal on 02/10/14.
 */
public class ReportType {
    private int id;
    private String name;
    private boolean privy;
    private String created_at;
    private String updated_at;

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPrivy() {
        return privy;
    }

    public void setPrivy(boolean privy) {
        this.privy = privy;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

}
