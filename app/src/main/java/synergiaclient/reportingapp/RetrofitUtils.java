package synergiaclient.reportingapp;

import retrofit.RestAdapter;

/**
 * Created by benjaminsenechal on 25/09/14.
 */
public class RetrofitUtils {
    private static RestAdapter restAdapter;

    public static RestAdapter GetRestAdapter() {
        restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://signalement.giesynergia.fr")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return restAdapter;
    }
}
