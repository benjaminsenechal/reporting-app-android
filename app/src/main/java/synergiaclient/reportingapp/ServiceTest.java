package synergiaclient.reportingapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;

/**
 * Created by benjaminsenechal on 25/09/14.
 */
public interface ServiceTest {
    @GET("/reports.json")
    void listReports(Callback<List<Report>> callback);

    @GET("/report_types.json")
    void listReportTypes(Callback<ArrayList<ReportType>> callback);

    @Headers({
        "Secure-Key: Reporting-App-Secure"
    })
    @Multipart
    @POST("/reports.json")
    void sendReport(@PartMap Map<String, Object> dictionary, Callback<String> callback);

}

