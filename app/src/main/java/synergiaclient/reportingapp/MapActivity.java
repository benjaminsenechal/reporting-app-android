package synergiaclient.reportingapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import android.location.LocationListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import android.location.LocationManager;
import android.location.Criteria;
import android.view.View;
import android.widget.Button;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MapActivity extends Activity implements LocationListener {
    private GoogleMap map=null;
    private static final int GPS_REQUEST_CODE = 1888;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    private Location location;
    private String provider;
    private static ServiceTest service;
    private  List<Report> reportsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkGPSActived();
        setContentView(R.layout.activity_map);
        this.setTitle("Signalements sur le réseau");

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        loadReports();

        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        } else {

            map = ((MapFragment) getFragmentManager()
                    .findFragmentById(R.id.map)).getMap();
            map.setMyLocationEnabled(true);

            locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

            if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            Criteria criteria = new Criteria();
            provider = locationManager.getBestProvider(criteria, false);

        }

        final Button button = (Button) findViewById(R.id.goToFormButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent activityChangeIntent = new Intent(MapActivity.this, FormActivity.class);
                MapActivity.this.startActivity(activityChangeIntent);
            }
        });
    }

    public void loadReports() {
        service = RetrofitUtils.GetRestAdapter().create(ServiceTest.class);
        service.listReports(new Callback<List<Report>>() {
            @Override
            public void success(List<Report> reports, Response response) {
                reportsList = reports;
                for (Report report : reportsList) {
                    Log.i("reportList",report.getCity().getName().toString());
                    LatLng l = new LatLng(report.getLatitude(), report.getLongitude());
                    map.addMarker(new MarkerOptions()
                            .title(report.getReportType().getName()+" - "+convertStateToString(report.getState()))
                            .snippet(report.getDescription())
                            .position(l)
                    );
                }
            }

            @Override
            public void failure(RetrofitError error) {
                String name = error.toString();
            }
        });
    }

    public String convertStateToString(String string) {
        if (string.equals("received"))
            return "Reçu";
        else if (string.equals("in_progress"))
            return "En cours de traitement";
        else if (string.equals("fence"))
            return "Clôturé";
        else if (string.equals("archived"))
            return "Archivé";
        else
            return "";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkGPSActived();
        Log.v("", "Resuming");
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    public void onLocationChanged(Location location) {
        Log.d("TAG", "GPSLocationChanged");
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        Log.d("TAG", "Received GPS request for " + String.valueOf(lat) + "," + String.valueOf(lng) + " , ready to rumble!");

        LatLng current = new LatLng(lat, lng);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 13));

        SharedPreferences preferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat("currentLatitudeUser", (float)location.getLatitude());
        editor.putFloat("currentLongitudeUser", (float)location.getLongitude());

        editor.commit();

    }

    private void checkGPSActived(){
        LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("GPS non disponible");
            dialog.setPositiveButton("Activer", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    paramDialogInterface.dismiss();
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, GPS_REQUEST_CODE);
                }
            });
            dialog.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    paramDialogInterface.dismiss();
                    finish();
                }
            });
            dialog.show();
        }
    }

    public void onProviderDisabled(String provider) { }

    public void onProviderEnabled(String provider) { }

    public void onStatusChanged(String provider, int status, Bundle extras) { }

}
